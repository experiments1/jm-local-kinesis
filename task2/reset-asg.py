import getopt, sys, boto3, time

short_options="hvn:"
long_options=["help","verbose","name="]

cmd_line=sys.argv[1:]
max_activity_retries=30
activity_retry_interval=10

verbose=False
asg_name=""

help_string="""
Rebuild instances in AWS autoscaling group one by one. 
AWS credentials need to be defined in your environment / credential file.
Script will abort if an instance takes more than 5min to terminate successfully.
Arguments:
   -h --help             Show this message
   -v --verbose          Show verbose output
   -n --name asg_name    Name of the ASG 
"""

try:
    parsedArgs, unparsedArgs = getopt.getopt(cmd_line,short_options,long_options)
except getopt.error as err:
    print(str(err))
    print(help_string)
    sys.exit(2)

for option, arg in parsedArgs:
    if option in ['-h','--help']:
        print(help_string)
        sys.exit(0)
    elif option in ['-v','--verbose']:
        verbose=True
    elif option in ['-n','--name']:
        asg_name = arg

if asg_name.length==0:
    print("Name of ASG to reset must be specified.")
    print(help_string)
    sys.exit(0)

asg_client = boto3.client("autoscaling")
 
group_data = asg_client.describe_auto_scaling_groups(
    AutoScalingGroupNames=[
        asg_name,
    ]
 )

if group_data['AutoScalingGroups'].length==0:
    print(f"Autoscaling group with name '{asg_name}' not found.")
    sys.exit(0) 

if group_data['AutoScalingGroups'].length>1:
    print(f"Multiple autoscaling group with name '{asg_name}' found, unclear which group to target.")
    sys.exit(0) 

instance_data=group_data['AutoScalingGroups'][0]['Instances']

for instance in instance_data:
    if verbose:
        print(f"Terminating instance {instance['InstanceId']}\n")
    asg_activity = asg_client.terminate_instance_in_auto_scaling_group(
        InstanceId=instance['InstanceId'],
        ShouldDecrementDesiredCapacity=False
    )
    activity_retries=0
    while asg_activity['StatusCode']!='Success':
        time.sleep(activity_retry_interval)
        updated_activities=asg_client.describe_scaling_activities(
            ActivityIds=[
               asg_activity['ActivityId'],
            ],
            AutoScalingGroupName=asg_name
        )
        asg_activity=updated_activities['Activities'][0]
        activity_retries=activity_retries+1
        if activity_retries > max_activity_retries:
            print(f"Instance {instance['InstanceId']}, took more than {activity_retry_interval*max_activity_retries}sec to terminate, aborting script\n Last action status was '{asg_activity['StatusCode']}'")
            sys.exit(2)
    if verbose:
        print(f"Terminated instance {instance['InstanceId']} in {activity_retries*activity_retry_interval}sec\n")    
        