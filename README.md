#Kinesis on Local stack

Creates a kinesis stream with firehose to s3 bucket on localstack running in docker.
Terrafrom is used to deploy all resources.
Python and boto3 to write random data to the kinesis stream

##Requirements
To run this code you will need docker and python 3 installed. 
On windows you can run the run_scripts.ps1 powershell script to start localstack, deploy the infra and post random data to the stream. Once you have run this script and localstack is running you can post further random data by running 'python send-data.py'
I have not had time to write an equivalent bash script to the powershell script but one could be written.
Also presently references to the docker network jm-local-kinesis are hardcoded, you will need to be running from a directory called jm-local-kinesis for the powershell script to work. This is to be fixed.  