provider "aws" {
  access_key                  = "mock_access_key"
  region                      = "us-east-1"
  s3_force_path_style         = true
  secret_key                  = "mock_secret_key"
  skip_credentials_validation = true
  skip_metadata_api_check     = true
  skip_requesting_account_id  = true

  endpoints {
    firehose       = "http://localstack:4573"
    iam            = "http://localstack:4593"
    kinesis        = "http://localstack:4568"
    s3             = "http://localstack:4572"
   }
}


resource "aws_kinesis_stream" "kenesis_stream" {
  name             = "random-stream"
  shard_count      = 1
  retention_period = 48

 
  tags = {
    Environment = "local"
  }
}

resource "aws_s3_bucket" "kinesis_bucket" {
  bucket = "kinesis-bucket"
  acl    = "private"

  tags = {
    Name        = "kinesis-bucket"
    Environment = "local"
  }
}


resource "aws_kinesis_firehose_delivery_stream" "test_stream" {
  name        = "kinesis-firehose-data-stream"
  destination = "s3"

  s3_configuration {
    role_arn   = "${aws_iam_role.firehose_role.arn}"
    bucket_arn = "${aws_s3_bucket.kinesis_bucket.arn}"
  }
}