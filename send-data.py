import boto3
from random import seed
from random import random

seed()

kinesis_client = boto3.client(service_name="kinesis", endpoint_url="http://localhost:4568")
randomData = random()

response = kinesis_client.put_record(
    StreamName='random-stream',
    Data=str(randomData),
    PartitionKey='1'
)

print(response)



