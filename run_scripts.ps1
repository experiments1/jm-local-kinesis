$env:SERVICES="s3,kinesis,firehose,iam"
docker-compose up -d
$run_terraform_base_params='run','--net','jm-local-kinesis_default','--volume',"${pwd}:/src",'--workdir','/src','--entrypoint','terraform','hashicorp/terraform:0.12.2'

docker ($run_terraform_base_params + 'init')
docker ($run_terraform_base_params + 'validate')
docker ($run_terraform_base_params + 'plan', '-out', 'tf.plan')
docker ($run_terraform_base_params + 'apply', '-auto-approve', 'tf.plan')

#docker build -t jm-kinesis-test .
#docker run --net jm-local-kinesis_default jm-kinesis-test
python send-data.py